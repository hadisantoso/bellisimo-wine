<?php

namespace App\Http\Controllers;

use App\Models\Message;
use Illuminate\Http\Request;
use App\Models\HomeSlider;
use App\Models\BrandStory;
use App\Models\Product;
use App\Models\Order;
use App\Models\Event;
use App\Models\Testimonial;
use App\Models\Article;
use App\Models\Newsletter;
use Mail;
class HomeController extends Controller
{
    public function index(Request $request){
      $homesliders = HomeSlider::orderBy('order_by','asc')->get();
      $brandstory = BrandStory::first();
      $products = Product::all();
      $events = Event::orderBy('date','desc')->get();
      $testimonials = Testimonial::all();
      $articles = Article::orderBy('published_date','desc')->get();
      return view('index',['homesliders' => $homesliders,'brandstory' => $brandstory,
            'products' => $products, 'events' => $events, 'testimonials' => $testimonials,
            'articles' => $articles
      ]);
    }

    public function order(Request $request){
      Order::create($request->all());
      return redirect()->route('home')->with('message','Thanks For Ordering...');
    }

    public function newsletter(Request $request){
      Newsletter::create($request->all());
      $data = array('email'=>$request->email);
      Mail::send('emails.welcome',$data, function ($message) use ($data) {
          $message->to($data['email'])->subject('Welcome to Bellisimo Wine');
      });
      return redirect()->route('home')->with('message','Thanks for subscribe');
    }

    public function newsdetail1(Request $request){
      return view('news-detail1');
    }
    public function newsdetail2(Request $request){
      return view('news-detail2');
    }
    public function newsdetail3(Request $request){
      return view('news-detail3');
    }

    public function message(Request $request){
        Message::create($request->all());
        $data = array('email'=>$request->email);
        Mail::send('emails.welcome',$data, function ($message) use ($data) {
            $message->to($data['email'])->subject('Welcome to Bellissimo Wine');
        });
        return redirect()->route('home')->with('message','Thanks for Contacting Us');
    }

    public function articleDetail($slug){
        $article = Article::where('slug',$slug)->first();
        return view('article',['article' => $article]);
    }
}
