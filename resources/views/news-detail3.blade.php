@include('include.header-meta')

    <!-- NAVBAR -->
    <nav class="navbar navbar-expand-lg fixed-top" id="navbarcustom">
      <a class="navbar-brand" href="{{ url('/') }}"><img src="{{ url('/') }}/img/logo-bellissimo-sparkling.png" class="logo-bellissimo-wines" alt="Bellissimo Wines"></a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"><img src="{{ url('/') }}img/icon-menu.png" class="icon-menu" alt="Bellissimo Wines"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item">
            <a class="nav-link page-scroll" href="{{ url('/') }}"><strong>BACK TO HOME</strong></a>
          </li>
         
        </ul>
      </div>
    </nav>
    <!-- HEADER NEWS DETAIL -->
    <div class="container-fluid">
      <div class="row">
          <img src="{{ url('/') }}/img/news/slider-news.jpg" class="header-news-detail" alt="Bellissimo Wines">
      </div>
    </div>

    <!-- EVENT -->
    <div class="container-fluid animatedParent" data-sequence='500' id="news-detail">
      <div class="container">
        <div class="row">
          <div class="col-lg-6 offset-lg-3 text-center">
            <div class="event animated bounceInDown" data-id='1'>
              <h1>Bellissimo Sparkling Launching Soiree</h1>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-8 offset-lg-2 text-center">
            <div class="news-gallery">
              <div><img src="{{ url('/') }}/img/news/news-thumb-launching-1.jpg" class="event-thumb" alt="Bellissimo Wines"></div>
              <div><img src="{{ url('/') }}/img/news/news-thumb-launching-2.jpg" class="event-thumb" alt="Bellissimo Wines"></div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-8 offset-lg-2">
            <div class="news-paragraph">
                <p>The speech and cheers ritual on stage by Mr. Irman CEO  of Dima International Wines marks the official launch of Bellissimo Sparkling Wines. Bellissimo Sparkling Dazzling Launching Soiree that was held in BAE by Socieaty in Jakarta, has drawn a capacity crowd of 150 customers showed up. Bellissimo creates wines that are delicious expressions of what we love to drink. Our varieties and wine styles are classic Italian icons, our Australian vineyards produce lusciously flavoursome wine grapes, and our winemakers labour lovingly over our wines to produce things of true beauty. “Our name Bellissimo (meaning “Beautiful” in Italian), sums it all up quite well really. Moderate in alcohol level, balanced beautifully with the perfect amount of natural sweetness, finishing with mouth tingling sparkles, combined with delicious aromas, Bellissimo wines are highly desirable for any occasion.” , said Mitch Hayhow as the Winemaker of Bellissimo Wines.</p>
            </div>
          </div>
        </div>
      </div>
    </div>

@include('include.footer')
    <script>
      $(window).on('load',function(){
          $('#popupVerificationAge').modal('show');
      });

      $(document).ready(function(){
          var $header = $('#header');//Caching element
          var $nav = $('.navbar');//Caching element
          // hide .navbar first - you can also do this in css .nav{display:none;}
          // $header.hide();
          $nav.css("background", "transparent");

          // fade in .navbar
          $(function () {
              $(window).scroll(function () {
                  // set distance user needs to scroll before we start fadeIn
                  if ($(this).scrollTop() > 200) { //For dynamic effect use $nav.height() instead of '100'
                      $nav.css("background", "rgba(255,255,255,0.8)");
                      // $header.fadeIn();
                      // $nav.fadeIn();
                  } else {
                      $nav.css("background", "linear-gradient(to top, rgba(255,255,255,0), rgba(255,255,255,1)");
                      // $header.fadeOut();
                      // $nav.fadeOut();
                  }
              });
          });
      });
      $(document).ready(function(){
          var $header = $('#header');//Caching element
          var $nav = $('.floating-whatsapp');//Caching element
          // hide .navbar first - you can also do this in css .nav{display:none;}
          $header.hide();
          $nav.hide();

          // fade in .navbar
          $(function () {
              $(window).scroll(function () {
                  // set distance user needs to scroll before we start fadeIn
                  if ($(this).scrollTop() > 100) { //For dynamic effect use $nav.height() instead of '100'
                      $header.fadeIn();
                      $nav.fadeIn();
                  } else {
                      $header.fadeOut();
                      $nav.fadeOut();
                  }
              });
          });
      });
      $('.carousel').carousel({
        interval: 3000
      })

      $(document).ready(function() {
        var name = $("#txtname").val();
        var email = $("#txtemail").val();
        var phone = $("#txtphone").val();
        var product = $("#txtproduct").val();
        
        $('#txtname').keyup(function() {var name = $(this).val();});
        $('#txtemail').keyup(function() {var email = $(this).val();});
        $('#txtphone').keyup(function() {var phone = $(this).val();});
        $('#txtproduct').keyup(function() {var product = $(this).val();});

        var url = 'https://api.whatsapp.com/send?phone=6281297932212&text=';
        var api = url+'Hi%20Bellissimo,%20I%20want%20to%20order%20';
        // var api = url+'Hi%20Bellissimo,%20My%20Name%20'+name+'%20('+email+'-'+phone+')'+'%20I%20want%20to%20order%20'+product;
        
        $(".api-whatsapp").append('<a href="'+api+'"><button type="button" class="btn btn-whatsapp desktop">Talk to us via Whatsapp &nbsp;&nbsp;&nbsp;&nbsp; <img src="https://bellissimowines.com/img/icon-whatsapp.png" class="icon-whatsapp" alt="Bellissimo Wines"></button></a>');
        $(".api-whatsapp").append('<a href="'+api+'"><button type="button" class="btn btn-whatsapp-mobile mobile">Talk to us via Whatsapp<br><img src="https://bellissimowines.com/img/icon-whatsapp.png" class="icon-whatsapp" alt="Bellissimo Wines"></button></a>');

      });

      $('body').scrollspy({ target: '#navbarcustom', offset: 120 })
      
      $(document).ready(function(){
        $('.news-gallery').slick({
          dots: true,
          infinite: true,
          speed: 500,
          fade: true,
          cssEase: 'linear'
        });
      });
    </script>
@include('include.footer-meta')