<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href={{ URL::asset("css/style.css") }}>

    <title>Bellissimo Wines</title>
  </head>
  <body>
    <!-- HEADER -->
    <div class="container-fluid" id="header">
      <div class="container">
        <div class="row">
          <div class="col-lg-6">
            <div class="header">
              ORDER NOW:  (+62) 123 4567 896
            </div>
          </div>
          <div class="col-lg-3 text-right">
            <div class="header">
              <div class="social-header">
                <img src="img/icon-fb.png" alt="Facebook Bellissimo Wines">
                <img src="img/icon-ig.png" alt="Instagram Bellissimo Wines">
              </div>
            </div>
          </div>
          <div class="col-lg-3">
            <div class="input-group">
              <input class="form-control search-box" placeholder="Search..">
              <div class="input-group-addon" ><img src="img/icon-search.png" class="icon-search"></div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- NAVBAR -->
    <nav class="navbar navbar-expand-lg fixed-top">
      <a class="navbar-brand" href="#"><img src="img/logo-bellissimo-wines.png" class="logo-bellissimo-wines"></a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item active">
            <a class="nav-link" href="#header">HOME <span class="sr-only">(current)</span></a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#brand-story">BRAND STORY</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#products">PRODUCTS</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#order">ORDER NOW</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#events">EVENTS</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#blog">BLOG</a>
          </li>
        </ul>
      </div>
    </nav>

    <!-- HOME SLIDER -->
    @foreach($homesliders as $homeslider)
      <!-- Untuk image -->
      {{ asset("uploads/".$homeslider->background_img) }}
      <!-- untuk attribut lain bisa liat di db -->
    @endforeach

    <div class="container-fluid" id="home-slider">
      <div class="container">
        <div class="row">
          <div class="col-lg-6 text-center">
            <div class="headline">
              <img src="img/share-your-sparkle.png">
              <img src="img/divider.png" class="divider">
              <p>Bellissimo is a Sparkling Wine that blended from Margareth Rivers Grapes Australia in North Bali. A combination of perfect grapes and carbonation creating a refreshing drink with ABV 8%.</p>
              <button class="btn btn-warning">The Story Behind</button>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- BRAND STORY -->
    <div class="container-fluid" id="brand-story">
      {{$brandstory}}
      {{ asset("uploads/".$brandstory->background_img) }}
      <div class="container">
        <div class="row">
          <div class="col-lg-6">
            <div class="brand-story">
              <img src="img/img-brand-story.png">
            </div>
          </div>
          <div class="col-lg-6">
            <div class="brand-story">
              <h1>Brand Story</h1>
              <h4>INSPIRED BY ITALY, GROWN IN<br>AUSTRALIA, LOVED BY INDONESIA.</h4>
              <p>Bellissimo creates wines that are delicious expressions of what we love to drink. Our varieties and wine styles are classic Italian icons, our Australian vineyards produce lusciously flavoursome wine grapes, and our winemakers labour lovingly over our wines to produce things of true beauty.</p>
              <p>Our name Bellissimo (meaning "Beautiful" in Italian), sums it all up quite well really. Moderate in alcohol level, balanced beautifully with the perfect amount of natural sweetness, finishing with mouth tingling sparkles, combined with delicious aromas, Bellissimo wines are highly desirable for any occasion.</p>
              <p>Share a little Sparkle wherever you go… </p>
              <button class="btn btn-warning">Explore Product</button>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- SHARE -->
    <div class="container-fluid" id="share">
      <div class="container">
        <div class="row">
          <div class="col-lg-4 offset-lg-4 text-center">
            <div class="share">
              <h1>Share a Little Spark</h1>
              <img src="img/divider.png" class="divider">
              <div class="social-share">
                <img src="img/icon-fb.png" alt="Facebook Bellissimo Wines">
                <img src="img/icon-ig.png" alt="Instagram Bellissimo Wines">
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- PRODUCTS -->
    @foreach($products as $product)
      {{$products}}
      <!-- image asset -->
      {{ asset("uploads/".$product->image) }}
    @endforeach
    <div class="container-fluid" id="products">
      <div class="container">
        <div class="row">
          <div class="col-lg-4 offset-lg-4 text-center">
            <div class="our-products">
              <h1>Our Products</h1>
              <h4>BLENDED WITH LOVE</h4>
              <img src="img/divider.png" class="divider">
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-5 offset-lg-1">
            <div class="products">
              <div class="products-box-left">
                <div class="products-name-left">
                  <h4>Moscato</h4>
                  <h4><span>IDR 300K</span>
                </div>
              </div>
              <img src="img/product-moscato.png" class="product-img-left" alt="Moscato">
              <div class="clear-25"></div>
              <button class="btn btn-warning tasting-note-btn">View full <strong>Tasting Note</strong></button>
            </div>
          </div>
          <div class="col-lg-5">
            <div class="products">
              <div class="products-box-right">
                <div class="products-name-right">
                  <h4>Dolce Rosso</h4>
                  <h4><span>IDR 300K</span>
                </div>
              </div>
              <img src="img/product-dolce-rosso.png" class="product-img-right" alt="Dolce Rosso">
              <div class="clear-25"></div>
              <button class="btn btn-warning tasting-note-btn" >View full <strong>Tasting Note</strong></button>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- ORDER -->
    <div class="container-fluid" id="order">
      <div class="container">
        <div class="row">
          <div class="col-lg-4 offset-lg-4 text-center">
            <div class="order">
              <h1>Open for Oder!</h1>
              <h4>WE WILL SERVED IT RIGHT TO YOUR DOOR</h4>
              <img src="img/divider.png" class="divider">
            </div>
          </div>
        </div>
        <form class="contact-form" action="{{ route('order') }}" method="post">
          {{ csrf_field() }}
        <div class="row">
          <div class="col-lg-5 offset-lg-1">
              <div class="form-group">
                <input type="text" class="form-control" name="name" placeholder="Name" required>
              </div>
              <div class="form-group">
                <input type="email" class="form-control" name="email" placeholder="Email" required>
              </div>
          </div>
          <div class="col-lg-5">
              <div class="form-group">
                <input type="text" class="form-control" name="phone_number" placeholder="Phone Number" required>
              </div>
              <select name="product" class="form-group form-control" required>
                <option value="">---Select Product---</option>
                @foreach($products as $product)
                  <option value="{{$product->name}}">{{$product->name}}</option>
                @endforeach
              </select>
          </div>
          <div class="col-lg-4 offset-lg-4">
            <div class="clear-25"></div>
            <button type="submit" class="btn btn-warning tasting-note-btn"  >COMPLETE YOUR ORDER</button>
          </div>
        </div>
      </form>

      </div>
    </div>

    <!-- EVENT -->
    @foreach($events as $event)
      {{$event}}
    @endforeach
    <div class="container-fluid" id="event">
      <div class="container">
        <div class="row">
          <div class="col-lg-4 offset-lg-4 text-center">
            <div class="event">
              <h1>Latest Events</h1>
              <h4>YOU DON'T WANT TO MISS</h4>
              <img src="img/divider.png" class="divider">
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-4">
            <div class="event-box">
              <img src="img/event-thumb.png" class="event-thumb">
              <h4>Bellisimo Sparkling Moscatol Party</h4>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
              <p class="time"><img src="img/icon-time.png"> Posted 12 Mar 2018 10:00 am</p>
            </div>
          </div>
          <div class="col-lg-4">
            <div class="event-box">
              <img src="img/event-thumb.png" class="event-thumb">
              <h4>Bellisimo Sparkling Moscatol Party</h4>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </p>
              <p class="time"><img src="img/icon-time.png"> Posted 12 Mar 2018 10:00 am</p>
            </div>
          </div>
          <div class="col-lg-4">
            <div class="event-box">
              <img src="img/event-thumb.png" class="event-thumb">
              <h4>Bellisimo Sparkling Moscatol Party</h4>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </p>
              <p class="time"><img src="img/icon-time.png"> Posted 12 Mar 2018 10:00 am</p>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- TESTIMONIAL -->
    @foreach($testimonials as $testimonial)
    {{$testimonial}}
    @endforeach
    <div class="container-fluid" id="testimonial">
      <div class="container">
        <div class="row">
          <div class="col-lg-4 offset-lg-4 text-center">
            <div class="testimonial">
              <h1>Our Customer Says</h1>
              <h4>TESTIMONIALS</h4>
              <img src="img/divider.png" class="divider">
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-6 offset-lg-3 text-center">
            <div class="testimonial">
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </p>
              <img src="img/icon-quote.png" class="icon-quote">
              <h4>Deden Sembada</h4>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- BLOG -->
    @foreach($articles as $article)
    {{$article}}
    @endforeach
    <div class="container-fluid" id="blog">
      <div class="container">
        <div class="row">
          <div class="col-lg-4 offset-lg-4 text-center">
            <div class="blog">
              <h1>Latest News</h1>
              <h4>ARTICLE</h4>
              <img src="img/divider.png" class="divider">
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-6">
            <div class="blog-box">
              <img src="img/article-thumb.png" class="blog-thumb">
              <h4>Bellisimo Sparkling Moscatol Party</h4>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
              <p class="time"><img src="img/icon-time.png"> Posted 12 Mar 2018 10:00 am</p>
              <button class="btn btn-warning">See More</button>
            </div>
          </div>
          <div class="col-lg-6">
            <div class="blog-box">
              <img src="img/article-thumb.png" class="blog-thumb">
              <h4>Bellisimo Sparkling Moscatol Party</h4>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </p>
              <p class="time"><img src="img/icon-time.png"> Posted 12 Mar 2018 10:00 am</p>
              <button class="btn btn-warning">See More</button>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- NEWSLETTER -->
    <div class="container-fluid" id="newsletter">
      <div class="container">
        <div class="row">
          <div class="col-lg-2">
            <div class="newsletter">
              <h4>NEWSLETTER</h4>
            </div>
          </div>
          <div class="col-lg-6">
            <div class="newsletter"><p>Sign up with your email to get updates fresh updates about our events</p></div>
          </div>

            <div class="col-lg-4">
              <div class="newsletter">
                <form action="{{ route('newsletter') }}" method="post">
                  {{ csrf_field() }}
                <div class="input-group">
                  <input class="form-control search-box" type="email" name="email" placeholder="Email..">
                  <button class="btn btn-warning">SIGN UP</button>
                </div>
                </form>
              </div>
            </div>

        </div>
      </div>
    </div>

    <!-- FOOTER -->
    <div class="container-fluid" id="footer">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 text-center">
            <div class="footer">
              @ Copyright 2018. All Right Reserved.
            </div>
          </div>
        </div>
      </div>
    </div>

    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  </body>
  <script>
  @if(session()->has('message'))
      alert('{{ session()->get('message') }}');
  @endif
  </script>
</html>
