@include('include.header-meta')
@include('include.header')

    <!-- POPUP VERIFICATION AGE -->
    <div class="modal" tabindex="-1" role="dialog" id="popupVerificationAge" data-backdrop="static" data-keyboard="false">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-body">
            <div class="container">
              <div class="row">
                <div class="col-lg-12 text-center">
                  <div class="clear-25"></div>
                  <img src="img/logo-bellissimowines.png" class="logo-bellissimo-wines" alt="Bellissimo Wines Logo">
                  <hr>
                  <p>To experience this site you must be of legal drinking age.</p>
                  <!-- <p>Enter your birthdate:</p>
                  <p><input type="text" name="birthday" value="" id="basic"/></p>
                  <hr> -->
                  <button type="button" class="btn btn-secondary" data-dismiss="modal" id="enter">YES I'M 21+</button>
                  <div class="clear-25"></div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- SLIDER FULL SCREEN -->
    <header class="desktop">
      <div id="carouselExampleIndicators" class="carousel carousel-fade slide" data-ride="carousel">
        <ol class="carousel-indicators">
          <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
          <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
          <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
          <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
          <li data-target="#carouselExampleIndicators" data-slide-to="4"></li>
        </ol>
        <div class="carousel-inner" role="listbox">
          <div class="carousel-item active" id="home-slider">
            <div class="container">
              <div class="row">
                <div class="col-lg-6 text-center">
                  <div class="headline">
                    <div class="brand-story">
                      <h1>SHARE YOUR SPARKLE</h1>
                    </div>
                    <p>Bellissimo is a Sparkling Wine that blended from Margareth Rivers Grapes Australia in North Bali. A combination of perfect grapes and carbonation creating a refreshing drink with ABV 8%.</p>
                    <button class="btn btn-warning">Find Out More</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
          @foreach($homesliders as $homeslider)
          <div class="carousel-item" style="background-image: url('uploads/{{$homeslider->background_img}}')"></div>
          @endforeach
        </div>
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
      </div>
    </header>

    <header class="mobile">
      <div id="carouselExampleIndicatorsMobile" class="carousel carousel-fade slide" data-ride="carousel">
        <ol class="carousel-indicators">
          <li data-target="#carouselExampleIndicatorsMobile" data-slide-to="0" class="active"></li>
          <li data-target="#carouselExampleIndicatorsMobile" data-slide-to="1"></li>
          <li data-target="#carouselExampleIndicatorsMobile" data-slide-to="2"></li>
          <li data-target="#carouselExampleIndicatorsMobile" data-slide-to="3"></li>
          <li data-target="#carouselExampleIndicatorsMobile" data-slide-to="4"></li>
        </ol>
        <div class="carousel-inner" role="listbox">
           <div class="carousel-item active" id="home-slider">
            <div class="container">
              <div class="row">
                <div class="col-lg-6 text-center">
                  <div class="headline">
                    <div class="brand-story">
                      <h1>SHARE YOUR SPARKLE</h1>
                    </div>
                    <img src="img/divider.png" class="divider">
                    <p>Bellissimo is a Sparkling Wine that blended from Margareth Rivers Grapes Australia in North Bali. A combination of perfect grapes and carbonation creating a refreshing drink with ABV 8%.</p>
                    <button class="btn btn-warning">Find Out More</button>
                  </div>
                </div>
              </div>
            </div>
           </div>
          <div class="carousel-item" style="background-image: url('img/slider-6-mobile.jpg')" alt="Bellissimo Wines"></div>
          <div class="carousel-item" style="background-image: url('img/slider-7-mobile.jpg')" alt="Bellissimo Wines"></div>
          <div class="carousel-item" style="background-image: url('img/slider-8-mobile.jpg')" alt="Bellissimo Wines"></div>
          <div class="carousel-item" style="background-image: url('img/slider-5-mobile.jpg')" alt="Bellissimo Wines"></div>

        </div>
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
      </div>
    </header>

    <!-- BRAND STORY -->
    <div class="container-fluid animatedParent" data-sequence='500' id="brand-story">
      <div class="container">
        <div class="row">
          <div class="col-lg-4">
            <div class="brand-story animated bounceInLeft" data-id='1'>
              <img src="img/brand-story-thumb.png" alt="Bellissimo Wines Our Story">
            </div>
          </div>
          <div class="col-lg-7">
            <div class="brand-story animated bounceInRight" data-id='2'>
              <h1>Our Story</h1>
              {!! $brandstory->desc !!}
              <div class="clear-25"></div>
            </div>
          </div>
        </div>
      </div>
      <img src="img/brand-story-images.jpg" class="brand-story-images animated bounceInUp" data-id='3' alt="Bellissimo Wines Our Story">
      <div class="clear-25"></div>
    </div>

    <!-- PRODUCTS -->
    <div class="container-fluid animatedParent" data-sequence='500' id="products">
      <div class="container">
        <div class="row">
          <div class="col-lg-4 offset-lg-4 text-center">
            <div class="our-products animated bounceInDown" data-id='1'>
              <h1>Our Collection</h1>
              <!-- <h4>BLENDED WITH LOVE</h4> -->
              <img src="img/divider.png" class="divider" alt="Bellissimo Wines Divider">
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-2 text-center animated bounceInLeft mobile" data-id='2'>
            <img src="img/product-moscato.png" class="product-img mobile" alt="Moscato">
          </div>
          <div class="col-lg-4 text-center">
            <div class="products-box animated bounceInLeft" data-id='2'>
              <h4>Moscato</h4>
              <p><strong>Grape Variety</strong>: <br>100% Muscat Blanc.</p>
              <p><strong>Appellation</strong> (Region): Premium selected vineyards within South Eastern Australia (fermented and bottled in Indonesia).</p> 
              <button class="btn btn-warning" data-toggle="modal" data-target="#modalProductMoscato">View Full <strong>Tasting Note</strong></button>
            </div>
          </div>
          <div class="col-lg-2 text-center animated bounceInLeft" data-id='2'>
            <img src="img/product-moscato.png" class="product-img desktop" alt="Moscato">
          </div>
          <div class="col-lg-2 text-center animated bounceInRight" data-id='2'>
            <img src="img/product-dolce-rosso.png" class="product-img" alt="Dolce Rosso">
          </div>
          <div class="col-lg-4 text-center">
            <div class="products-box animated bounceInRight" data-id='2'>
              <h4>Dolce Rosso</h4>
              <p><strong>Grape Variety</strong>: A blend of Grenache, Shiraz and Cabernet Sauvignon.</p>
              <p><strong>Appellation</strong> (Region): Premium selected vineyards within South Eastern Australia (fermented and bottled in Indonesia).</p> 
              <button class="btn btn-warning" data-toggle="modal" data-target="#modalProductDolceRosso">View Full <strong>Tasting Note</strong></button>
            </div>
          </div>
          
        </div>
      </div>
    </div>

    <!-- Modal Product Moscato -->
    <div class="modal fade bd-example-modal-lg" id="modalProductMoscato" tabindex="-1" role="dialog" aria-labelledby="modalProductMoscato" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header text-center">
            <h3 class="modal-title">Moscato</h3>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="container">
              <div class="row">
                <div class="col-lg-3 text-center">
                  <div class="modal-product">
                    <img src="img/product-moscato.png" class="product-img" alt="Moscato">
                  </div>
                </div>
                <div class="col-lg-9">
                  <div class="modal-product">
                    <p><strong>Grape Variety:</strong><br>100% Muscat Blanc</p> 
                    <p><strong>Appellation (Region):</strong><br>Premium selected vineyards within South Eastern Australia (fermented and bottled in Indonesia).</p>
                    <p><strong>Winemaking:</strong><br>De-stemmed, crushed, pressed and cold settled, racked as clarified juice then blast frozen. Partial fermentation is then carried out in stainless steel tanks, until approximately two thirds complete, when the fermentation is stopped by rapid chilling. The resulting high level of grape sugars gives the wine its sweetness, while the trapped bubbles from the fermentation are retained all the way through to bottle.</p>   
                    <p><strong>Sensory Notes:</strong><br>Nose:  Sweet Muscat and delicate white floral aromas combine with subtle lemon meringue.<br>Palate: Abundant residual grape sweetness balanced beautifully with mouth watering natural acidity, finishing with fine sparkles on the tongue.</p>
                    <p><strong>Food Ideas:</strong><br>Due to its sweetness and vibrant aromas, this Moscato is one of the most food friendly wines imaginable. From savoury appetisers though to a wide range of spicy main dishes and of course a huge range of deserts. Serve well chilled in a champagne flute.</p>
                    <a href="{{ url('/uploads/bellissimo-wines-tasting-notes.pdf') }}" target="_blank"><button class="btn-warning">Download Tasting Notes</button></a>
                    <div class="clear-25"></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Modal Product Dolce Rosso -->
    <div class="modal fade bd-example-modal-lg" id="modalProductDolceRosso" tabindex="-1" role="dialog" aria-labelledby="modalProductDolceRosso" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header text-center">
            <h3 class="modal-title">Dolce Rosso</h3>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="container">
              <div class="row">
                <div class="col-lg-3 text-center">
                  <div class="modal-product">
                    <img src="img/product-dolce-rosso.png" class="product-img" alt="Dolce Rosso">
                  </div>
                </div>
                <div class="col-lg-9">
                  <div class="modal-product">
                    <p><strong>Grape Variety:</strong><br>A blend of Grenache, Shiraz and Cabernet Sauvignon.</p>  
                    <p><strong>Appellation (Region):</strong> <br>Premium selected vineyards within South Eastern Australia (fermented and bottled in Indonesia).</p>
                    <p><strong>Winemaking:</strong><br>De-stemmed, crushed, then blast frozen. Primary fermentation on skins is then carried out in open fermenters to approximately two thirds complete. The must is then pressed and the fermentation is stopped by rapid chilling .The resulting high level of grape sugars gives the wine its sweetness, while the trapped bubbles from the fermentation are retained all the way through to bottle.</p>
                    <p><strong>Sensory Notes:</strong><br>Nose:  Sweet black cherry and red berry aromas combine with hints of plum.<br>Palate: Generous residual grape sweetness balanced beautifully with mouth watering natural acidity, finishing with fine tannins and sparkles on the tongue.</p>
                    <p><strong>Food Ideas:</strong><br>Due to its sweetness and vibrant aromas, this Dolce Rosso is a very versatile food friendly wine. Pairs well with richly flavoured appetisers though to a wide range of strongly flavoured spicy dishes, fatty meats and rich deserts. Serve well chilled in a champagne flute.</p>
                    <a href="https://bellissimowines.com/uploads/bellissimo-wines-tasting-notes.pdf" target="_blank"><button class="btn-warning">Download Tasting Notes</button></a>
                    <div class="clear-25"></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- TESTIMONIAL -->
    <div class="container-fluid animatedParent" data-sequence='500' id="testimonial">
      <div class="container">
        <div class="row">
          <div class="col-lg-6 offset-lg-3 text-center">
            <div class="testimonial animated bounceInDown" data-id='1'>
              <h1>Our Customer Say</h1>
              <h4>TESTIMONIALS</h4>
              <img src="img/divider.png" class="divider" alt="Bellissimo Wines Divider">
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-6 offset-lg-3 text-center">
            <div class="testimonial animated bounceInDown" data-id='2'>
              @foreach($testimonials as $testimonial)
              <div>
                <p>{{$testimonial->desc}}</p>
                <img src="img/icon-quote.png" class="icon-quote" alt="Bellissimo Wines">
                <h4>{{$testimonial->name}}</h4>
              </div>
              @endforeach
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- EVENT -->
    <div class="container-fluid animatedParent" data-sequence='500' id="events">
      <div class="container">
        <div class="row">
          <div class="col-lg-6 offset-lg-3 text-center">
            <div class="event animated bounceInDown" data-id='1'>
              <h1>News & Events</h1>
              <h4>Our Latest Updates</h4>
              <img src="img/divider.png" class="divider" alt="Bellissimo Wines">
            </div>
          </div>
        </div>
        <div class="row">
          @foreach($articles as $article)
          <div class="col-lg-4">
            <div class="event-box animated bounceInDown" data-id='2'>
              <img src="uploads/{{$article->thumbnail}}" class="event-thumb" alt="{{$article->title}}">
              <h4>{{$article->title}}</h4>
              <div class="time"><img src="img/icon-time.png" alt="Icon Time"> Posted {{date('d M Y', strtotime($article->published_date))}}</div>
              <p>{{$article->short_desc}}</p>
              <a href="{{ url('/article') }}/{{$article->slug}}"><button type="button" class="btn btn-warning tasting-note-btn">See More</button></a>
            </div>
          </div>
          @endforeach
        </div>
      </div>
    </div>

    <!-- ORDER -->
    <div class="container-fluid animatedParent" data-sequence="500" id="order">
      <div class="container">
        <div class="row">
          <div class="col-lg-4 offset-lg-4 text-center">
            <div class="order animated bounceInDown" data-id='1'>
              <h1>Order Our Products</h1>
              <h4>WE WILL SERVE RIGHT TO YOUR DOOR</h4>
              <img src="img/divider.png" class="divider" alt="Bellissimo Wines">
            </div>
          </div>
        </div>
        <form class="contact-form" action="{{ route('order') }}" method="post">
          {{ csrf_field() }}
          <div class="row animated bounceInDown" data-id='2'>
            <div class="col-lg-5 offset-lg-1">
                <div class="form-group">
                  <input type="text" class="form-control" placeholder="Name" name="txtname" id="txtname">
                </div>
                <div class="form-group">
                  <input type="email" class="form-control" placeholder="Email" name="txtemail" id="txtemail">
                </div>
            </div>
            <div class="col-lg-5">
                <div class="form-group">
                  <input type="text" class="form-control" placeholder="Phone Number" name="txtphone" id="txtphone">
                </div>
                <div class="form-group" name="txtproduct" id="txtproduct">
                  <select class="form-control">
                    <option value="-">-- Select Product --</option>
                    @foreach($products as $product)
                      <option value="{{$product->name}}">{{$product->name}}</option>
                    @endforeach
                  </select>
                </div>
            </div>
            <div class="col-lg-12 text-center">
              <div class="clear-25"></div>
              <div class="api-whatsapp"></div>
            </div>
          </div>
        </form>
      </div>
    </div>

    <!-- CONTACT -->
    <div class="container-fluid" id="contact">
      <div class="container">
        <div class="row">
          <div class="col-lg-5 text-center">
            <div class="contact">
              <h1>Contact Us</h1>
              <h4>SAY HELLO</h4>
              <img src="img/divider.png" class="divider" alt="Bellissimo Wines">
              <div class="clear-25"></div>
                <form action="{{ route('message') }}" method="post">
                    {{ csrf_field() }}
                    <div class="form-group">
                      <input type="text" class="form-control" placeholder="Name" name="name" required>
                    </div>
                    <div class="form-group">
                      <input type="email" class="form-control" placeholder="Email" name="email" required>
                    </div>
                    <div class="form-group">
                      <input type="text" class="form-control" placeholder="Subject" name="subject" required>
                    </div>
                    <div class="form-group">
                      <textarea class="form-control" rows="5" placeholder="message" name="message" required></textarea>
                    </div>
                    <div class="clear-25"></div>
                    <button type="submit" class="btn btn-warning tasting-note-btn">SUBMIT</button>
                </form>
            </div>
          </div>
        </div>
      </div>
    </div>

@include('include.footer')
<script>
      @if(session()->has('message'))
      $('#popupVerificationAge p').html('{{session()->get('message')}}');
      $('#popupVerificationAge #enter').html('YES');
      $('#popupVerificationAge').modal('show');
      @endif
      $(window).on('load',function(){
          checkCookie();
      });

      $(document).ready(function(){
          var $header = $('#header');//Caching element
          var $nav = $('.navbar');//Caching element
          // hide .navbar first - you can also do this in css .nav{display:none;}
          // $header.hide();
          $nav.css("background", "linear-gradient(to top, rgba(255,255,255,0.1), rgba(255,255,255,1)");

          // fade in .navbar
          $(function () {
              $(window).scroll(function () {
                  // set distance user needs to scroll before we start fadeIn
                  if ($(this).scrollTop() > 50) { //For dynamic effect use $nav.height() instead of '100'
                      $nav.css("background", "rgba(255,255,255,1)");
                      // $header.fadeIn();
                      // $nav.fadeIn();
                  } else {
                      $nav.css("background", "linear-gradient(to top, rgba(255,255,255,0.1), rgba(255,255,255,1)");
                      // $header.fadeOut();
                      // $nav.fadeOut();
                  }
              });
          });
      });
      $(document).ready(function(){
          var $header = $('#header');//Caching element
          var $nav = $('.floating-whatsapp');//Caching element
          // hide .navbar first - you can also do this in css .nav{display:none;}
          $header.hide();
          $nav.hide();

          // fade in .navbar
          $(function () {
              $(window).scroll(function () {
                  // set distance user needs to scroll before we start fadeIn
                  if ($(this).scrollTop() > 100) { //For dynamic effect use $nav.height() instead of '100'
                      $header.fadeIn();
                      $nav.fadeIn();
                  } else {
                      $header.fadeOut();
                      $nav.fadeOut();
                  }
              });
          });
      });
      $('.carousel').carousel({
        interval: 3000
      })

      $(document).ready(function() {
        var name = $("#txtname").val();
        var email = $("#txtemail").val();
        var phone = $("#txtphone").val();
        var product = $("#txtproduct").val();

        $('#txtname').keyup(function() {var name = $(this).val();});
        $('#txtemail').keyup(function() {var email = $(this).val();});
        $('#txtphone').keyup(function() {var phone = $(this).val();});
        $('#txtproduct').keyup(function() {var product = $(this).val();});
        var api ="";
        if(isMobile()){
            var url = 'https://api.whatsapp.com/send?phone=6281297932212&text=';
        }else{
            var url = 'https://web.whatsapp.com/send?phone=6281297932212&text=';

        }
        var api = url+'Hi%20Bellissimo,%20I%20want%20to%20order%20';
        // var api = url+'Hi%20Bellissimo,%20My%20Name%20'+name+'%20('+email+'-'+phone+')'+'%20I%20want%20to%20order%20'+product;

        $(".api-whatsapp").append('<a href="'+api+'"><button type="button" class="btn btn-whatsapp desktop">Talk to us via Whatsapp &nbsp;&nbsp;&nbsp;&nbsp; <img src="img/icon-whatsapp.png" class="icon-whatsapp" alt="Bellissimo Wines"></button></a>');
        $(".api-whatsapp").append('<a href="'+api+'"><button type="button" class="btn btn-whatsapp-mobile mobile">Talk to us via Whatsapp<br><img src="img/icon-whatsapp.png" class="icon-whatsapp" alt="Bellissimo Wines"></button></a>');
      });

      $('body').scrollspy({ target: '#navbarcustom', offset: 120 })

      function isMobile() {
          if( navigator.userAgent.match(/Android/i)
              || navigator.userAgent.match(/webOS/i)
              || navigator.userAgent.match(/iPhone/i)
              || navigator.userAgent.match(/iPad/i)
              || navigator.userAgent.match(/iPod/i)
              || navigator.userAgent.match(/BlackBerry/i)
              || navigator.userAgent.match(/Windows Phone/i)
          ){
              return true;
          }
          else {
              return false;
          }
      }

      $('.testimonial').slick({
        dots: true,
        infinite: true,
        speed: 500,
        fade: false,
        cssEase: 'linear'
      });

      $('#enter').click(function () {
          setCookie('verify_age','1',10);
      });


      function setCookie(cname,cvalue,exdays) {
          var d = new Date();
          d.setTime(d.getTime() + (exdays*24*60*60*1000));
          var expires = "expires=" + d.toGMTString();
          document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
      }

      function getCookie(cname) {
          var name = cname + "=";
          var decodedCookie = decodeURIComponent(document.cookie);
          var ca = decodedCookie.split(';');
          for(var i = 0; i < ca.length; i++) {
              var c = ca[i];
              while (c.charAt(0) == ' ') {
                  c = c.substring(1);
              }
              if (c.indexOf(name) == 0) {
                  return c.substring(name.length, c.length);
              }
          }
          return "";
      }

      function checkCookie() {
          var age=getCookie("verify_age");
          if (age === "") {
              $('#popupVerificationAge').modal('show');
          }
      }
    </script>
@include('include.footer-meta')