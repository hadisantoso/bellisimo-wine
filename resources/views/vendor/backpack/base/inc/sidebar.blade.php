@if (Auth::check())
    <!-- Left side column. contains the sidebar -->
    <aside class="main-sidebar">
      <!-- sidebar: style can be found in sidebar.less -->
      <section class="sidebar">
        <!-- Sidebar user panel -->
        @include('backpack::inc.sidebar_user_panel')

        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
          {{-- <li class="header">{{ trans('backpack::base.administration') }}</li> --}}
          <!-- ================================================ -->
          <!-- ==== Recommended place for admin menu items ==== -->
          <!-- ================================================ -->
          <li><a href="{{ backpack_url('dashboard') }}"><i class="fa fa-dashboard"></i> <span>{{ trans('backpack::base.dashboard') }}</span></a></li>
          <li><a href="{{ backpack_url('homeslider') }}"><i class="fa fa-dashboard"></i> <span>Home Sliders</span></a></li>
          <li><a href="{{ backpack_url('brandstory') }}"><i class="fa fa-dashboard"></i> <span>Brand Stories</span></a></li>
          <li><a href="{{ backpack_url('product') }}"><i class="fa fa-dashboard"></i> <span>Products</span></a></li>
          <li><a href="{{ backpack_url('social') }}"><i class="fa fa-dashboard"></i> <span>Social Link</span></a></li>
          <li><a href="{{ backpack_url('order') }}"><i class="fa fa-dashboard"></i> <span>Orders</span></a></li>
          <li><a href="{{ backpack_url('event') }}"><i class="fa fa-dashboard"></i> <span>Events</span></a></li>
          <li><a href="{{ backpack_url('testimonial') }}"><i class="fa fa-dashboard"></i> <span>Testimonials</span></a></li>
          <li><a href="{{ backpack_url('article') }}"><i class="fa fa-dashboard"></i> <span>Articles</span></a></li>
          <li><a href="{{ backpack_url('newsletter') }}"><i class="fa fa-dashboard"></i> <span>Newsletters</span></a></li>
          <li><a href="{{ backpack_url('message') }}"><i class="fa fa-dashboard"></i> <span>Messages</span></a></li>




          <!-- ======================================= -->
          {{-- <li class="header">Other menus</li> --}}
        </ul>
      </section>
      <!-- /.sidebar -->
    </aside>
@endif
