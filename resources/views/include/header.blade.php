<!-- NAVBAR -->
<nav class="navbar navbar-expand-lg fixed-top" id="navbarcustom">
  <a class="navbar-brand" href="{{ url('/') }}"><img src="img/logo-bellissimo-wines.png" class="logo-bellissimo-icon" alt="Bellissimo Wines Logo Icon"><img src="img/logo-bellissimo-sparkling.png" class="logo-bellissimo-wines" alt="Bellissimo Wines Logo"></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"><img src="img/icon-menu.png" class="icon-menu" alt="Icon Menu"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav ml-auto">
      <li class="nav-item">
        <a class="nav-link page-scroll" href="#home">HOME <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link page-scroll" href="#brand-story">OUR STORY</a>
      </li>
      <li class="nav-item">
        <a class="nav-link page-scroll" href="#products">OUR COLLECTION</a>
      </li>
      <li class="nav-item">
        <a class="nav-link page-scroll" href="#events">NEWS & EVENTS</a>
      </li>
      <li class="nav-item">
        <a class="nav-link page-scroll" href="#order">ORDER</a>
      </li>
      <li class="nav-item">
        <a class="nav-link page-scroll" href="#contact">CONTACT US</a>
      </li>
    </ul>
  </div>
</nav>