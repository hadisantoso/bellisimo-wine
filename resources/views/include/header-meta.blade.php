<!doctype html>
<html lang="en-US" class="no-js">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Bellissimo is a Sparkling Wine that blended from Margareth Rivers Grapes Australia in North Bali. A combination of perfect grapes and carbonation creating a refreshing drink with ABV 8%" />
    <meta name="keywords" content="Bellissimo, bellissimo wines, wines, sparkling wines, wine, jenis wine terkenal" />

    <link rel="shortcut icon" type="image/png" href="{{ url('/') }}/img/favicon.png"/>
    <link async="async" rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    
    <!-- <link href="css/animations.css" rel="stylesheet" > -->
    <link async="async" href="css/scrolling-nav.css" rel="stylesheet">
    <link async="async" rel="stylesheet" type="text/css" href="{{ url('/') }}/slick/slick.css"/>
    <link async="async" rel="stylesheet" type="text/css" href="{{ url('/') }}/slick/slick-theme.css"/>
    <link async="async" href="{{ url('/') }}/css/style.css" rel="stylesheet" >

    <title>Bellissimo Wines</title>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-110760363-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-110760363-1');
    </script>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <script src="{{ url('/') }}/js/jquery.easing.min.js"></script>
    <script src="{{ url('/') }}/js/scrolling-nav.js"></script>
    <script src="{{ url('/') }}/js/css3-animate-it.js"></script>
    <script type="text/javascript" src="{{ url('/') }}/slick/slick.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    
  </head>
  <body id="home">
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TJM9DF5"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->