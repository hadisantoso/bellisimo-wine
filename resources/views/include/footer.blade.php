<!-- NEWSLETTER -->
<!-- NEWSLETTER -->
    <div class="container-fluid" id="newsletter">
      <div class="clear-25 mobile"></div>
      <div class="container">
        <div class="row">
          <div class="col-lg-2">
            <div class="newsletter">
              <h4>NEWSLETTER</h4>
            </div>
          </div>
          <div class="col-lg-6">
            <div class="newsletter"><p>Sign up with your email to get updates fresh updates about our events</p></div>
          </div>
          <div class="col-lg-4">
            <div class="newsletter">
              <form action="{{ route('newsletter') }}" method="post">
                {{ csrf_field() }}
                <div class="input-group">
                  <input class="form-control search-box" placeholder="Email.." type="email" name="email">
                  <button type="submit" class="btn btn-black">SIGN UP</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
      <div class="clear-25 mobile"></div>
    </div>

    <!-- FOOTER -->
    <div class="container-fluid" id="footer">
      <div class="footer">
        <div class="container">
          <div class="row">
            <div class="col-lg-12 text-center">
              <h1>Find Us</h1>
              <h5>WHERE TO BUY OUR PRODUCTS</h5>
              <div class="clear-25"></div>
            </div>
          </div>
          <div class="row">
            <div class="col-lg-4">
              <p><strong>Jakarta</strong></p>
              <p>Komp. Pluit Mas Blok BB No.3-4<br>Jembatan Tiga, Pluit, Jakarta Utara 14450<br> Phone/Email: +62(21) 6618835/julia.delly@dima.co.id<br>astipoeri.handarto@dima.co.id</p>
              <p>Le Vintage Wine Shop - Jakarta<br>Lottemart Kemang La Codefin<br>Jl. Kemang Raya No. 8, Jakarta 12730, Indonesia.<br>Phone: +62(21) 7183568</p>
            </div>
            <div class="col-lg-4">
              <p><strong>Jakarta</strong></p>
              <p>Lottemart Fatmawati Ground Floor<br>Jl. RS. Fatmawati No. 15. Komp. Goldern Fatmawati,<br>Jakarta 12420, Indonesia.<br>Phone: +62(21) 7591 0874</p>
              <p>Lottemart Gandaria City, Lower Ground<br>Jl. Sultan Iskandar Muda, Kebayoran Lama.<br>Jakarta Selatan 12240, Indonesia.<br>Phone: +62(21) 2905 3048</p>
            </div>
            <div class="col-lg-4">
              <p><strong>Bali</strong></p>
              <p>Jl. Mahendradrata No 41<br>Pemecutan Klod, Denpasar Barat, Tegal Harum<br>Denpasar, Bali 80119<br>Phone/Email: +62(36) 1486758/roy.mulyadi@dima.co.id</p>
              
              <p><strong>Surabaya</strong></p>
              <p>Le Vintage Wine Shop - Surabaya<br>Lobby Lounge of Shangri-La Hotel Jl. Mayjend. Sungkono<br>No. 120. Surabaya, Jawa Timur 60256, Indonesia<br>Phone: +62(31) 5661610 Ext. 6268</p>
            </div>
          </div>
          <div class="row">
            <div class="col-lg-12 text-center">
              <p>@ Copyright 2018. All Right Reserved.</p>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Floating Whatsapp -->
    <div id="floating" class="mobile">
      <a href="https://api.whatsapp.com/send?phone=6281297932212&text=Hi%20Bellissimo...">
        <div class="floating-whatsapp">
          Talk to us via Whatsapp <img src="img/icon-whatsapp.png" class="icon-whatsapp-floating">
        </div>
      </a>
    </div>