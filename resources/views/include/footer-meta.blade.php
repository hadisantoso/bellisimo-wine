    <script type="application/ld+json">
      {
        "@context": "http://schema.org",
        "@type": "Organization",
        "url": "https://bellissimowines.com",
        "logo": "https://bellissimowines.com/img/logo-bellissimo.png",
        "contactPoint": [
          { "@type": "ContactPoint",
            "telephone": "+6281297932212",
            "contactType": "Customer Service"
          }
        ],
        "sameAs": [
          "https://instagram.com/bellissimowines"
        ]
      }

    </script>
  </body>
</html>