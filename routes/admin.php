<?php

// Backpack\CRUD: Define the resources for the entities you want to CRUD.
CRUD::resource('homeslider', 'HomeSliderCrudController');
CRUD::resource('brandstory', 'BrandStoryCrudController');
CRUD::resource('product', 'ProductCrudController');
CRUD::resource('social', 'SocialCrudController');
CRUD::resource('order', 'OrderCrudController');
CRUD::resource('event', 'EventCrudController');
CRUD::resource('testimonial', 'TestimonialCrudController');
CRUD::resource('article', 'ArticleCrudController');
CRUD::resource('newsletter', 'NewsletterCrudController');
CRUD::resource('message', 'MessageCrudController');

