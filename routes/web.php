<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','HomeController@index')->name('home');
Route::get('/news/food-hotel-and-tourism-bali-2018','HomeController@newsdetail1');
Route::get('/news/media-appreciating-evening','HomeController@newsdetail2');
Route::get('/news/bellissimo-sparkling-launching-soiree','HomeController@newsdetail3');
Route::post('/order','HomeController@order')->name('order');
Route::post('/newsletter','HomeController@newsletter')->name('newsletter');
Route::post('/message','HomeController@message')->name('message');

Route::get('/article/{slug}','HomeController@articleDetail')->name('article');

Route::get('/phpinfo',function(){
  return view('phpinfo');
});

Route::post('admin/media-dropzone', 'Admin\ArticleCrudController@handleDropzoneUpload');


Route::get('/clear-cache', function() {
    $exitCode = Artisan::call('cache:clear');
    return '<h1>Cache facade value cleared</h1>';
});

//Clear Config cache:
Route::get('/config-cache', function() {
    $exitCode = Artisan::call('config:cache');
    return '<h1>Clear Config cleared</h1>';
});